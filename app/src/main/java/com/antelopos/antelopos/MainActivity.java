package com.antelopos.antelopos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.antelopos.antelopos.adapter.ActiveOrderAdapter;
import com.antelopos.antelopos.adapter.CategoryAdapter;
import com.antelopos.antelopos.adapter.ProductAdapter;
import com.antelopos.antelopos.fragment.order.OrderActiveFragment;
import com.antelopos.antelopos.fragment.order.OrderCheckoutFragment;
import com.antelopos.antelopos.fragment.order.OrderFragment;
import com.antelopos.antelopos.fragment.order.OrderLoadingFragment;
import com.antelopos.antelopos.model.Category;
import com.antelopos.antelopos.model.Order;
import com.antelopos.antelopos.model.OrderItem;
import com.antelopos.antelopos.model.Product;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements
        OrderActiveFragment.ActiveOrderListener,
        OrderFragment.OrderListener,
        OrderCheckoutFragment.CheckoutListener{
    public ArrayList<Order> orders = new ArrayList<>();
    public ArrayList<String> categories = new ArrayList<>();
    public ArrayList<Product> products = new ArrayList<>();

    public ActiveOrderAdapter activeOrderAdapter;
    public ProductAdapter productAdapter;
    public CategoryAdapter categoryAdapter;

    HashMap<String, ArrayList<Product>> categoryProductHashMap = new HashMap<>();
    HashMap<String, Product> productHashMap = new HashMap<>();

    FragmentManager fragManager;

    OrderActiveFragment orderActiveFragment = new OrderActiveFragment();
    OrderFragment orderFragment = new OrderFragment();
    OrderCheckoutFragment orderCheckoutFragment = new OrderCheckoutFragment();

    private String ROOT_STACK = "root_stack";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragManager = getSupportFragmentManager();
        fragManager.beginTransaction()
                .replace(R.id.mainFrame, new OrderLoadingFragment(), "Loading")
                .commit();

        loadData(savedInstanceState);
    }

    private void loadDefaultFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null)
            defaultFragment();
    }

    private void loadData(final Bundle savedInstanceState) {
        categoryAdapter = new CategoryAdapter(categories);
        productAdapter = new ProductAdapter(products);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference catalogRef = database.getReference("category");
        DatabaseReference productRef = database.getReference("product");

        catalogRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                categories.clear();

                for (DataSnapshot noteSnapshot : dataSnapshot.getChildren()) {
                    String category = noteSnapshot.getValue(Category.class).getName();
                    categories.add(category);
                }

                categoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        productRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                categoryProductHashMap.clear();
                productHashMap.clear();
                products.clear();

                for (DataSnapshot noteSnapshot : dataSnapshot.getChildren()) {
                    Product product = noteSnapshot.getValue(Product.class);

                    if (categoryProductHashMap.containsKey(product.getCategory())) {
                        categoryProductHashMap.get(product.getCategory()).add(product);
                    } else {
                        ArrayList<Product> pTemp = new ArrayList<>();
                        pTemp.add(product);
                        categoryProductHashMap.put(product.getCategory(), pTemp);
                    }

                    productHashMap.put(product.getName(), product);
                    products.add(product);
                }
                loadDataDummy(savedInstanceState);
                productAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void loadDataDummy(Bundle savedInstanceState) {
        if (orders.size() <= 0) {
            ArrayList<OrderItem> orderItems = new ArrayList<>();
            orderItems.add(new OrderItem(products.get(0), 2));
            orderItems.add(new OrderItem(products.get(1), 2));

            orders.add(new Order(0, 1, "Andika", true, orderItems));
            orders.add(new Order(1, 2, "Anang", true));
            orders.add(new Order(2, 3, "Dika", true));
            orders.add(new Order(3, 4, "Nanang", true));

        }

        activeOrderAdapter = new ActiveOrderAdapter(orders);

        loadDefaultFragment(savedInstanceState);
    }

    private void defaultFragment() {
        orderActiveFragment.setActiveOrderListener(this);

        fragManager.beginTransaction()
                .replace(R.id.mainFrame, orderActiveFragment, orderActiveFragment.toString())
                .commit();
    }


    private void switchFragment(Fragment fragment, String tag) {
        fragManager.beginTransaction()
                .replace(R.id.mainFrame, fragment, fragment.toString())
                .addToBackStack(tag)
                .commit();
    }

    @Override
    public void OnCreateNewOrder(String name, int no) {
        Bundle bundle = new Bundle();
        //TODO: Order ID
        bundle.putString("orderNewName", name);
        bundle.putInt("orderNewNo", no);

        orderFragment.setArguments(bundle);
        orderFragment.setOrderListener(this);

        switchFragment(orderFragment, ROOT_STACK);
    }

    @Override
    public void OnLoadOrder(Order order) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("order", order);

        orderFragment.setArguments(bundle);
        orderFragment.setOrderListener(this);

        switchFragment(orderFragment, ROOT_STACK);
    }

    @Override
    public void OnSaveClose(Order order) {
        if (!orders.contains(order))
            orders.add(order);

        fragManager.popBackStackImmediate();
        activeOrderAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnPrint(Order order) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("order", order);

        orderCheckoutFragment.setArguments(bundle);

        switchFragment(orderCheckoutFragment, ROOT_STACK);
    }

    @Override
    public void OnCheckOut(Order order) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("order", order);
        bundle.putString("checkout", "checkout");

        orderCheckoutFragment.setArguments(bundle);
        orderCheckoutFragment.setCheckoutListener(this);

        switchFragment(orderCheckoutFragment, null);
    }

    @Override
    public void OnCheckout(Order order) {
        order.setActive(false);

        clearBackStack();
        fragManager.popBackStack();
        activeOrderAdapter.notifyDataSetChanged();
    }

    private void clearBackStack() {
        if (fragManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = fragManager.getBackStackEntryAt(0);
            fragManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }
}
