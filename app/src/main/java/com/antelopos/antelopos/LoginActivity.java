package com.antelopos.antelopos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.antelopos.antelopos.fragment.login.LoginForgotFragment;
import com.antelopos.antelopos.fragment.login.LoginFragment;

public class LoginActivity extends AppCompatActivity
        implements
        LoginFragment.LoginListener,
        LoginForgotFragment.ForgotListener {
    FragmentManager loginFragManager;

    FrameLayout loginFrame;

    LoginFragment loginFragment = new LoginFragment();
    LoginForgotFragment loginForgotFragment = new LoginForgotFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginFragment.setLoginListener(this);
        loginForgotFragment.setForgotListener(this);

        loginFragManager = getSupportFragmentManager();
        loginFragManager.beginTransaction()
                .replace(R.id.loginFrame, loginFragment)
                .commit();
    }

    @Override
    public void OnLogin(String email, String pass) {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void OnForgot() {
        loginFragManager.beginTransaction()
                .replace(R.id.loginFrame, loginForgotFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void OnForgot(String email) {

    }

    @Override
    public void OnBack() {
        loginFragManager.popBackStack();
    }
}
