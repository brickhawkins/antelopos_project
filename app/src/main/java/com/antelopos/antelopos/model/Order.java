package com.antelopos.antelopos.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Brick Hawkins on 16/08/2017.
 */

public class Order implements Serializable{
    private int id, no;
    private String name;
    private ArrayList<OrderItem> orderItems = new ArrayList<>();
    private int total = 0;
    private boolean active = false;

    public Order() {
    }

    public Order(int id, int no, String name, boolean active) {
        this.id = id;
        this.no = no;
        this.name = name;
        this.active = active;
    }

    public Order(int id, int no, String name, boolean active, ArrayList<OrderItem> orderItems) {
        this.id = id;
        this.no = no;
        this.name = name;
        this.active = active;
        this.orderItems = orderItems;

        if(orderItems.size() > 0)
            calculateTotal();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void addOrderItem(OrderItem orderItem){
        orderItems.add(orderItem);
        calculateTotal();
    }

    public void removeOrderItem(OrderItem orderItem){
        orderItems.remove(orderItem);
        calculateTotal();
    }

    public void calculateTotal(){
        this.total = 0;
        for(OrderItem item: orderItems){
            this.total += item.getItemTotalPrize();
        }
    }
}
