package com.antelopos.antelopos.model;

import java.io.Serializable;

/**
 * Created by Brick Hawkins on 16/08/2017.
 */

public class Product implements Serializable {
    private int id, quantity, price, cost;
    private String name, category;

    public Product() {
    }

    public Product(int id, int quantity, String name, String category, int price, int cost) {
        this.id = id;
        this.quantity = quantity;
        this.name = name;
        this.category = category;
        this.price = price;
        this.cost = cost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCost() { return cost; }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
