package com.antelopos.antelopos.model;

import java.io.Serializable;

/**
 * Created by Brick Hawkins on 22/08/2017.
 */

public class OrderItem implements Serializable{
    private Product product;
    private int quantity, ItemTotalPrize;

    public OrderItem(){

    }

    public OrderItem(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
        this.ItemTotalPrize = product.getPrice() * quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
        this.ItemTotalPrize = product.getPrice() * quantity;
    }

    public float getItemTotalPrize() {
        return ItemTotalPrize;
    }
}
