package com.antelopos.antelopos.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.antelopos.antelopos.R;
import com.antelopos.antelopos.adapter.view.OrderItemViewHolder;
import com.antelopos.antelopos.model.OrderItem;

import java.util.List;

/**
 * Created by Brick Hawkins on 01/09/2017.
 */

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemViewHolder> {
    private List<OrderItem> orderItems;
    private OrderItemListener orderItemListener;

    public OrderItemAdapter() {

    }

    public OrderItemAdapter(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public OrderItemAdapter(List<OrderItem> orderItems, OrderItemListener orderItemListener) {
        this.orderItems = orderItems;
        this.orderItemListener = orderItemListener;
    }

    @Override
    public OrderItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_item, parent, false);
        return new OrderItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderItemViewHolder holder, int position) {
        final OrderItem orderItem = orderItems.get(position);

        holder.name.setText(orderItem.getProduct().getName());
        holder.quantity.setText(orderItem.getQuantity() + " X " + orderItem.getProduct().getPrice());
        holder.total.setText(String.valueOf(orderItem.getItemTotalPrize()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderItemListener != null)
                    orderItemListener.OnOrderItemClick(orderItem);
            }
        });

        if (position % 2 == 0)
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.antelo_1));
        else
            holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.antelo_2));
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    public interface OrderItemListener {
        void OnOrderItemClick(OrderItem orderItem);
    }
}
