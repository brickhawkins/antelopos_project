package com.antelopos.antelopos.adapter.view;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.antelopos.antelopos.R;

/**
 * Created by Brick Hawkins on 21/08/2017.
 */

public class CategoryViewHolder extends RecyclerView.ViewHolder{
    public TextView categoryName;
    private FrameLayout selector;
    private int index;

    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    public CategoryViewHolder(View itemView) {
        super(itemView);

        categoryName = itemView.findViewById(R.id.categoryName);
        selector = itemView.findViewById(R.id.categorySelected);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public void setSelected() {
        this.itemView.setSelected(true);
        selector.setVisibility(View.VISIBLE);
    }

    public void setUnselected(){
        this.itemView.setSelected(false);
        selector.setVisibility(View.INVISIBLE);
    }
}
