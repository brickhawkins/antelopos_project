package com.antelopos.antelopos.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.antelopos.antelopos.R;
import com.antelopos.antelopos.adapter.view.ProductViewHolder;
import com.antelopos.antelopos.model.Product;

import java.util.List;

/**
 * Created by Brick Hawkins on 16/08/2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductViewHolder> {
    private List<Product> products;
    private ProductItemListener productItemListener;

    public ProductAdapter(List<Product> products) {
        this.products = products;
    }

    public void setProductItemListener(ProductItemListener productItemListener) {
        this.productItemListener = productItemListener;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_product_item, parent, false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        final Product product = products.get(position);

        holder.productName.setText(product.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Product", product.getName());
                productItemListener.OnProductClick(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public interface ProductItemListener{
        void OnProductClick(Product product);
    }
}
