package com.antelopos.antelopos.adapter.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.antelopos.antelopos.R;

/**
 * Created by Brick Hawkins on 11/09/2017.
 */

public class OrderItemViewHolder extends RecyclerView.ViewHolder {
    public TextView name, quantity, total;

    public OrderItemViewHolder(View itemView) {
        super(itemView);

        name = itemView.findViewById(R.id.itemName);
        quantity = itemView.findViewById(R.id.itemCount);
        total = itemView.findViewById(R.id.totalItemCost);
    }
}
