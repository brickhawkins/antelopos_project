package com.antelopos.antelopos.adapter.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.antelopos.antelopos.R;

/**
 * Created by Brick Hawkins on 16/08/2017.
 */

public class ProductViewHolder extends RecyclerView.ViewHolder {
    public TextView productName;

    public ProductViewHolder(View itemView) {
        super(itemView);

        productName = itemView.findViewById(R.id.productName);
    }
}
