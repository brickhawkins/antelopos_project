package com.antelopos.antelopos.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.antelopos.antelopos.R;
import com.antelopos.antelopos.adapter.view.ActiveOrderViewHolder;
import com.antelopos.antelopos.model.Order;

import java.util.List;

/**
 * Created by Brick Hawkins on 19/08/2017.
 */

public class ActiveOrderAdapter extends RecyclerView.Adapter<ActiveOrderViewHolder> {
    private ActiveOrderListener activeOrderListener;
    private List<Order> orders;

    public ActiveOrderAdapter(List<Order> orders) {
        this.orders = orders;
    }

    public void setActiveOrderListener(ActiveOrderListener activeOrderListener) {
        this.activeOrderListener = activeOrderListener;
    }

    @Override
    public ActiveOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_order_active, parent, false);

        return new ActiveOrderViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ActiveOrderViewHolder holder, int position) {
        final Order order = orders.get(position);

        holder.name.setText(order.getName());
        holder.no.setText(String.valueOf(order.getNo()));
        holder.total.setText(String.valueOf(order.getTotal()));

        if(order.isActive()) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activeOrderListener.OnActiveOrderClick(order);
                }
            });
        }else {
            holder.itemView.setBackgroundResource(R.drawable.rectangle_accent);
        }
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public interface ActiveOrderListener {
        void OnActiveOrderClick(Order order);
    }
}
