package com.antelopos.antelopos.adapter.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.antelopos.antelopos.R;

/**
 * Created by Brick Hawkins on 01/09/2017.
 */

public class ActiveOrderViewHolder extends RecyclerView.ViewHolder{
    public TextView name, no, total;

    public ActiveOrderViewHolder(View itemView) {
        super(itemView);

        name = (TextView) itemView.findViewById(R.id.orderName);
        no = (TextView) itemView.findViewById(R.id.orderNo);
        total = (TextView) itemView.findViewById(R.id.orderTotalCost);
    }
}
