package com.antelopos.antelopos.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.antelopos.antelopos.R;
import com.antelopos.antelopos.adapter.view.CategoryViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brick Hawkins on 21/08/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {
    private List<String> categories;
    private ArrayList<CategoryViewHolder> items = new ArrayList<>();
    private int selectedItem = 0;
    private CategoryClickListener categoryListener;

    public CategoryAdapter(List<String> categories) {
        this.categories = categories;
    }

    public void setCategoryListener(CategoryClickListener categoryListener) {
        this.categoryListener = categoryListener;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_category_item, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(view);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, int position) {
        final String categoryString = categories.get(position);

        items.add(holder);

        holder.categoryName.setText(categoryString);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedItem = holder.getAdapterPosition();
                updateSelection();
                categoryListener.OnCategoryClick(categoryString);
            }
        });

        if(selectedItem == holder.getAdapterPosition()) {
            holder.setSelected();
            categoryListener.OnCategoryClick(categoryString);
        }
    }

    private void updateSelection(){
        for(CategoryViewHolder holder: items){
            if(selectedItem == holder.getAdapterPosition()) {
                holder.setSelected();
            }else {
                holder.setUnselected();
            }
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public interface CategoryClickListener {
        void OnCategoryClick(String category);
    }
}