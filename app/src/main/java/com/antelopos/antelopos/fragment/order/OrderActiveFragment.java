package com.antelopos.antelopos.fragment.order;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.antelopos.antelopos.MainActivity;
import com.antelopos.antelopos.R;
import com.antelopos.antelopos.adapter.ActiveOrderAdapter;
import com.antelopos.antelopos.fragment.dialog.CreateOrderDialog;
import com.antelopos.antelopos.model.Order;

import java.util.ArrayList;

public class OrderActiveFragment extends Fragment
        implements
        View.OnClickListener,
        CreateOrderDialog.OrderDialogListener,
        ActiveOrderAdapter.ActiveOrderListener {

    View orderActiveView;
    SearchView searchOrder;
    FrameLayout addOrder;
    RecyclerView activeOrderGrid;
    ActiveOrderListener activeOrderListener;

    MainActivity main;

    public void setActiveOrderListener(ActiveOrderListener listener) {
        this.activeOrderListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        orderActiveView = inflater.inflate(R.layout.fragment_order_active, container, false);

        searchOrder = (SearchView) orderActiveView.findViewById(R.id.searchOrder);
        addOrder = (FrameLayout) orderActiveView.findViewById(R.id.addOrder);
        activeOrderGrid = (RecyclerView) orderActiveView.findViewById(R.id.activeOrderGrid);

        main = (MainActivity) container.getContext();

        main.activeOrderAdapter.setActiveOrderListener(this);

        activeOrderGrid.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        activeOrderGrid.setItemAnimator(new DefaultItemAnimator());
        activeOrderGrid.setAdapter(main.activeOrderAdapter);

        addOrder.setOnClickListener(this);

        initSearchView();

        return orderActiveView;
    }

    void initSearchView() {
        searchOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchOrder.setIconified(false);
            }
        });

        searchOrder.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchClear();
                return false;
            }
        });

        searchOrder.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchOrder(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    private void searchOrder(String query) {
        ArrayList<Order> searchOrder = new ArrayList<>();
        for(Order queryOrder: main.orders){
            if(queryOrder.getName().trim().toLowerCase().contains(query.trim().toLowerCase())){
                searchOrder.add((queryOrder));
            }
        }

        main.activeOrderAdapter = new ActiveOrderAdapter(searchOrder);
        main.activeOrderAdapter.setActiveOrderListener(this);

        activeOrderGrid.setAdapter(main.activeOrderAdapter);

        main.activeOrderAdapter.notifyDataSetChanged();
    }

    private void searchClear(){
        main.activeOrderAdapter = new ActiveOrderAdapter(main.orders);
        main.activeOrderAdapter.setActiveOrderListener(this);

        activeOrderGrid.setAdapter(main.activeOrderAdapter);

        main.activeOrderAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        CreateOrderDialog createOrder = new CreateOrderDialog();
        createOrder.show(getFragmentManager(), "createOrder");
        createOrder.setOrderDialogListener(this);
    }

    @Override
    public void OnCreateOrder(String name, int no) {
        activeOrderListener.OnCreateNewOrder(name, no);
    }

    @Override
    public void OnActiveOrderClick(Order order) {
        activeOrderListener.OnLoadOrder(order);
    }

    public interface ActiveOrderListener {
        void OnCreateNewOrder(String name, int no);

        void OnLoadOrder(Order order);
    }
}
