package com.antelopos.antelopos.fragment.order;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.antelopos.antelopos.R;
import com.antelopos.antelopos.adapter.OrderItemAdapter;
import com.antelopos.antelopos.fragment.dialog.PrintDialog;
import com.antelopos.antelopos.model.Order;
import com.antelopos.antelopos.model.OrderItem;

import java.util.Calendar;


public class OrderCheckoutFragment extends Fragment {
    Order checkoutOrder;
    OrderItem orderItem;

    TextView subTotal, discountValue, discount, total, payment, change, customerName, tableNo, date;
    TextView subTotalRight, discountValueRight, discountRight, totalRight, paymentRight, changeRight;
    FrameLayout back, print, checkout;
    RecyclerView orderNote;

    int discountPercent = 10;
    int paymentCash = 100000;

    OrderItemAdapter orderItemAdapter;
    CheckoutListener checkoutListener;

    public void setCheckoutListener(CheckoutListener checkoutListener) {
        this.checkoutListener = checkoutListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_order_checkout, container, false);

        subTotal = view.findViewById(R.id.checkoutSubtotal);
        discountValue = view.findViewById(R.id.checkoutDiscountValue);
        discount = view.findViewById(R.id.checkoutDiscount);
        total = view.findViewById(R.id.checkoutTotal);
        payment = view.findViewById(R.id.checkoutPayment);
        change = view.findViewById(R.id.checkoutChange);

        subTotalRight = view.findViewById(R.id.checkoutSubtotalRight);
        discountValueRight = view.findViewById(R.id.checkoutDiscountValueRight);
        discountRight = view.findViewById(R.id.checkoutDiscountRight);
        totalRight = view.findViewById(R.id.checkoutTotalRight);
        paymentRight = view.findViewById(R.id.checkoutPaymentRight);
        changeRight = view.findViewById(R.id.checkoutChangeRight);

        customerName = view.findViewById(R.id.orderName);
        tableNo = view.findViewById(R.id.orderNo);
        date = view.findViewById(R.id.orderDate);

        print = view.findViewById(R.id.printCheckout);
        back = view.findViewById(R.id.backCheckout);
        checkout = view.findViewById(R.id.checkoutOrder);

        orderNote = view.findViewById(R.id.orderNote);

        Bundle bundle = getArguments();

        if (bundle != null && bundle.containsKey("order")) {
            if(bundle.containsKey("checkout"))
                checkout.setVisibility(View.VISIBLE);
            else
                checkout.setVisibility(View.GONE);

            checkoutOrder = (Order) bundle.getSerializable("order");

            fillInformation();
        }

        initRecycleView(container);

        return view;
    }

    private void initRecycleView(ViewGroup container){
        orderItemAdapter = new OrderItemAdapter(checkoutOrder.getOrderItems());

        orderNote.setLayoutManager(new LinearLayoutManager(container.getContext(), LinearLayoutManager.VERTICAL, false));
        orderNote.setItemAnimator(new DefaultItemAnimator());
        orderNote.setAdapter(orderItemAdapter);

        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PrintDialog dialog = new PrintDialog();
                dialog.setCancelable(false);
                dialog.show(getFragmentManager(), "print");
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });

        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkoutListener.OnCheckout(checkoutOrder);
            }
        });
    }

    private void fillInformation(){
        customerName.setText(checkoutOrder.getName());
        tableNo.setText(String.valueOf(checkoutOrder.getNo()));

        String currentDate = DateFormat.format("dd/MM/yyyy", Calendar.getInstance().getTime()).toString();
        int disc = (checkoutOrder.getTotal() * discountPercent)/100;
        int discounted = checkoutOrder.getTotal() - disc;

        date.setText(currentDate);
        subTotal.setText(String.valueOf(checkoutOrder.getTotal()));
        discountValue.setText(String.valueOf(discountPercent));
        discount.setText(String.valueOf(disc));
        total.setText(String.valueOf(discounted));
        payment.setText(String.valueOf(paymentCash));
        change.setText(String.valueOf(paymentCash - discounted));

        subTotalRight.setText(String.valueOf(checkoutOrder.getTotal()));
        discountValueRight.setText(String.valueOf(discountPercent));
        discountRight.setText(String.valueOf(disc));
        totalRight.setText(String.valueOf(discounted));
        paymentRight.setText(String.valueOf(paymentCash));
        changeRight.setText(String.valueOf(paymentCash - discounted));
    }

    public interface CheckoutListener{
        void OnCheckout(Order order);
    }
}
