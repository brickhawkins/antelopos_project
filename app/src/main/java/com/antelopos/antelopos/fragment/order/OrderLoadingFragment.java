package com.antelopos.antelopos.fragment.order;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.antelopos.antelopos.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderLoadingFragment extends Fragment {


    public OrderLoadingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_loading, container, false);
    }

}
