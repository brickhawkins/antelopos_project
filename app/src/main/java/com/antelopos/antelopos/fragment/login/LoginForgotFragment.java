package com.antelopos.antelopos.fragment.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.antelopos.antelopos.R;

public class LoginForgotFragment extends Fragment {
    ForgotListener forgotListener;
    View forgotView;

    EditText email;
    FrameLayout reset, back;

    public void setForgotListener(ForgotListener forgotListener) {
        this.forgotListener = forgotListener;
    }

    public LoginForgotFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        forgotView = inflater.inflate(R.layout.fragment_login_forgot, container, false);

        email = forgotView.findViewById(R.id.inputEmail);
        reset = forgotView.findViewById(R.id.forgotButton);
        back = forgotView.findViewById(R.id.backToLogin);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotListener.OnBack();
            }
        });

        return forgotView;
    }

    public interface ForgotListener{
        void OnForgot(String email);
        void OnBack();
    }
}
