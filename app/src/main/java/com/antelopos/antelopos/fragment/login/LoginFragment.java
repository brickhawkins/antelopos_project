package com.antelopos.antelopos.fragment.login;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.antelopos.antelopos.R;

public class LoginFragment extends Fragment {
    LoginListener loginListener;

    FrameLayout signin, forgot;
    EditText email, password;
    View loginView;

    public LoginFragment() {
    }

    public void setLoginListener(LoginListener loginListener) {
        this.loginListener = loginListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        loginView = inflater.inflate(R.layout.fragment_login, container, false);

        signin = loginView.findViewById(R.id.loginButton);
        forgot = loginView.findViewById(R.id.goToForgot);
        email = loginView.findViewById(R.id.inputEmail);
        password = loginView.findViewById(R.id.inputPassword);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginListener.OnLogin(email.getText().toString(), password.getText().toString());
            }
        });

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginListener.OnForgot();
            }
        });

        return loginView;
    }

    public interface LoginListener {
        void OnLogin(String email, String pass);

        void OnForgot();
    }
}
