package com.antelopos.antelopos.fragment.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.antelopos.antelopos.R;
import com.antelopos.antelopos.model.OrderItem;

/**
 * Created by Brick Hawkins on 13/09/2017.
 */

public class OrderItemDialog extends DialogFragment {
    OrderItem orderItem = new OrderItem();
    OrderItemDialogListener orderItemDialogListener;
    FrameLayout add, subtract;
    TextView title, name, quantity, total;

    public void setOrderItemListener(OrderItemDialogListener orderItemDialogListener) {
        this.orderItemDialogListener = orderItemDialogListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_product_item_order, null);

        add = view.findViewById(R.id.addProductOrderItem);
        subtract = view.findViewById(R.id.subProductOrderItem);
        title = view.findViewById(R.id.orderItemDialogTitle);
        name = view.findViewById(R.id.nameProductOrderItem);
        quantity = view.findViewById(R.id.quantityProductOrderItem);
        total = view.findViewById(R.id.totalProductOrderItem);

        if (getArguments().getSerializable("orderItem") != null) {
            title.setText("Edit");
            this.orderItem = (OrderItem) getArguments().getSerializable("orderItem");

            name.setText(orderItem.getProduct().getName());
            quantity.setText(String.valueOf(orderItem.getQuantity()));
            total.setText(String.valueOf(orderItem.getItemTotalPrize()));

            builder.setView(view)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            editOrder();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Remove", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            removeOrder();
                            dialog.dismiss();
                        }
                    });
            ;
        } else if (getArguments().getSerializable("orderNewItem") != null) {
            title.setText("Add");
            this.orderItem = (OrderItem) getArguments().getSerializable("orderNewItem");

            name.setText(orderItem.getProduct().getName());
            quantity.setText(String.valueOf(orderItem.getQuantity()));
            total.setText(String.valueOf(orderItem.getItemTotalPrize()));

            builder.setView(view)
                    .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            addOrder();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            ;
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                orderItem.setQuantity(orderItem.getQuantity() + 1);
                quantity.setText(String.valueOf(orderItem.getQuantity()));
                total.setText(String.valueOf(orderItem.getItemTotalPrize()));
            }
        });

        subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (orderItem.getQuantity() > 1) {
                    orderItem.setQuantity(orderItem.getQuantity() - 1);
                    quantity.setText(String.valueOf(orderItem.getQuantity()));
                    total.setText(String.valueOf(orderItem.getItemTotalPrize()));
                }
            }
        });

        return builder.create();
    }

    void addOrder() {
        orderItemDialogListener.OnAdd(this.orderItem);
    }

    void editOrder() {
        orderItemDialogListener.OnEdit(this.orderItem);
    }

    void removeOrder() {
        orderItemDialogListener.OnRemove(this.orderItem);
    }

    public interface OrderItemDialogListener {
        void OnEdit(OrderItem orderItem);

        void OnAdd(OrderItem orderItem);

        void OnRemove(OrderItem orderItem);
    }
}
