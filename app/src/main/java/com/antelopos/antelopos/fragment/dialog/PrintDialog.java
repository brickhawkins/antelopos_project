package com.antelopos.antelopos.fragment.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.antelopos.antelopos.R;

/**
 * Created by Brick Hawkins on 17/09/2017.
 */

public class PrintDialog extends DialogFragment {
    TextView printStatus;
    AlertDialog.Builder builder;
    boolean donePrinting = false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate(R.layout.dialog_print, null);

        printStatus = view.findViewById(R.id.printStatus);
        printStatus.setText("CONNECTING PRINTER");

        donePrinting = false;

        builder.setView(view)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(donePrinting)
                            dialog.dismiss();
                    }
                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                printOrder(view);
            }
        }, 1000);

        return builder.create();
    }

    void printOrder(View view) {
        printStatus.setText("PRINTING");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                donePrintJob();
            }
        }, 1000);

    }

    void donePrintJob(){
        printStatus.setText("DONE");
        donePrinting = true;
    }
}
