package com.antelopos.antelopos.fragment.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.antelopos.antelopos.R;

/**
 * Created by Brick Hawkins on 13/09/2017.
 */

public class CreateOrderDialog extends DialogFragment {

    OrderDialogListener orderDialogListener;
    View view;

    EditText name, no;

    public void setOrderDialogListener(OrderDialogListener listener) {
        orderDialogListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.dialog_create_order, null))
                .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        name = getDialog().findViewById(R.id.createOrderName);
                        no = getDialog().findViewById(R.id.createOrderNo);

                        if (!name.getText().toString().isEmpty() && !no.getText().toString().isEmpty()) {
                            orderDialogListener.OnCreateOrder(
                                    name.getText().toString(),
                                    Integer.parseInt(no.getText().toString())
                            );
                            dialog.dismiss();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }

    public interface OrderDialogListener {
        void OnCreateOrder(String name, int no);
    }
}
