package com.antelopos.antelopos.fragment.order;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.antelopos.antelopos.MainActivity;
import com.antelopos.antelopos.R;
import com.antelopos.antelopos.adapter.CategoryAdapter;
import com.antelopos.antelopos.adapter.OrderItemAdapter;
import com.antelopos.antelopos.adapter.ProductAdapter;
import com.antelopos.antelopos.fragment.dialog.OrderItemDialog;
import com.antelopos.antelopos.model.Order;
import com.antelopos.antelopos.model.OrderItem;
import com.antelopos.antelopos.model.Product;

import java.util.ArrayList;


public class OrderFragment extends Fragment
        implements
        CategoryAdapter.CategoryClickListener,
        ProductAdapter.ProductItemListener,
        OrderItemAdapter.OrderItemListener,
        OrderItemDialog.OrderItemDialogListener {
    /*
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<Product> products = new ArrayList<>();
    HashMap<String, ArrayList<Product>> productHashMap = new HashMap<>();
    */
    OrderListener orderListener;
    ArrayList<OrderItem> orderItems;

    View orderView;

    RecyclerView productGrid;
    RecyclerView categoryTab;
    RecyclerView orderNote;

    TextView customerName, tableNo, totalOrder;
    FrameLayout saveCloseBtn, printBtn, checkoutBtn, searchFrame;
    ImageView searchButton;
    SearchView searchProduct;

    OrderItemAdapter orderItemAdapter;

    String currentCategory;
    Order currentOrder;
    MainActivity main;

    public OrderFragment() {
    }

    public void setOrderListener(OrderListener orderListener) {
        this.orderListener = orderListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        orderView = inflater.inflate(R.layout.fragment_order, container, false);

        customerName = orderView.findViewById(R.id.orderName);
        tableNo = orderView.findViewById(R.id.orderNo);
        totalOrder = orderView.findViewById(R.id.orderTotalCost);

        saveCloseBtn = orderView.findViewById(R.id.saveClose);
        printBtn = orderView.findViewById(R.id.print);
        checkoutBtn = orderView.findViewById(R.id.checkout);

        searchButton = orderView.findViewById(R.id.searchButton);
        searchFrame = orderView.findViewById(R.id.searchProductFrame);
        searchProduct = orderView.findViewById(R.id.searchProduct);

        initButton();
        initSearchView();
        initRecycleView(container);

        return orderView;
    }

    void initButton() {
        saveCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentOrder.setOrderItems(orderItems);
                orderListener.OnSaveClose(currentOrder);
            }
        });

        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentOrder.setOrderItems(orderItems);
                orderListener.OnPrint(currentOrder);
            }
        });

        checkoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentOrder.setOrderItems(orderItems);
                orderListener.OnCheckOut(currentOrder);
            }
        });
    }

    void initSearchView() {
        searchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchProduct.setIconified(false);
            }
        });

        searchProduct.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                searchClear();
                searchFrame.setVisibility(View.GONE);
                return false;
            }
        });

        searchProduct.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchProduct(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchFrame.getVisibility() == View.GONE)
                    searchFrame.setVisibility(View.VISIBLE);
                else
                    searchFrame.setVisibility(View.GONE);
            }
        });

    }

    void initRecycleView(ViewGroup container) {
        Bundle bundle = getArguments();

        if (bundle != null) {
            Log.d("Bundle", bundle.toString());

            if (bundle.containsKey("orderNewName")) {
                String orderName = (String) bundle.getString("orderNewName");
                int orderNo = (int) bundle.getInt("orderNewNo");

                //TODO: orderID
                currentOrder = new Order(0, orderNo, orderName, true);
                orderItems = new ArrayList<>();
                currentOrder.setOrderItems(orderItems);

                customerName.setText(orderName);
                tableNo.setText(String.valueOf(orderNo));
                totalOrder.setText("0");
            }
            if (bundle.containsKey("order")) {
                currentOrder = (Order) bundle.getSerializable("order");
                orderItems = currentOrder.getOrderItems();

                customerName.setText(currentOrder.getName());
                tableNo.setText(String.valueOf(currentOrder.getNo()));
                totalOrder.setText(String.valueOf(currentOrder.getTotal()));
            }
        }

        main = (MainActivity) container.getContext();

        //categoryAdapter = new CategoryAdapter(categories);
        main.categoryAdapter.setCategoryListener(this);

        categoryTab = orderView.findViewById(R.id.categoryTab);
        categoryTab.setLayoutManager(new LinearLayoutManager(container.getContext(), LinearLayoutManager.HORIZONTAL, false));
        categoryTab.setItemAnimator(new DefaultItemAnimator());
        categoryTab.setAdapter(main.categoryAdapter);

        //productAdapter = new ProductAdapter(products);
        main.productAdapter.setProductItemListener(this);

        productGrid = orderView.findViewById(R.id.productGrid);
        productGrid.setLayoutManager(new GridLayoutManager(container.getContext(), 3));
        productGrid.setItemAnimator(new DefaultItemAnimator());
        productGrid.setAdapter(main.productAdapter);

        orderItemAdapter = new OrderItemAdapter(orderItems, this);

        orderNote = orderView.findViewById(R.id.orderNote);
        orderNote.setLayoutManager(new LinearLayoutManager(container.getContext(), LinearLayoutManager.VERTICAL, false));
        orderNote.setItemAnimator(new DefaultItemAnimator());
        orderNote.setAdapter(orderItemAdapter);
    }

    private void searchProduct(String query) {
        ArrayList<Product> queryProducts = new ArrayList<>();
        for (Product product : main.products) {
            if (product.getName().trim().toLowerCase().contains(query.trim().toLowerCase())) {
                queryProducts.add(product);
            }
        }
        main.productAdapter = new ProductAdapter(queryProducts);
        main.productAdapter.setProductItemListener(this);

        productGrid.setAdapter(main.productAdapter);

        main.productAdapter.notifyDataSetChanged();
    }

    private void searchClear() {
        OnCategoryClick(currentCategory);
    }

    @Override
    public void OnProductClick(Product product) {
        boolean exist = false;
        Bundle bundle = new Bundle();

        for (int i = 0; i < currentOrder.getOrderItems().size(); i++) {
            if (currentOrder.getOrderItems().get(i).getProduct().equals(product)) {
                bundle.putSerializable("orderItem", currentOrder.getOrderItems().get(i));
                exist = true;
                break;
            }
        }

        if (!exist)
            bundle.putSerializable("orderNewItem", new OrderItem(product, 1));

        OrderItemDialog orderItemDialog = new OrderItemDialog();
        orderItemDialog.setOrderItemListener(this);
        orderItemDialog.setArguments(bundle);
        orderItemDialog.show(getFragmentManager(), "loadOrderItem");
    }

    @Override
    public void OnOrderItemClick(OrderItem orderItem) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("orderItem", orderItem);

        OrderItemDialog orderItemDialog = new OrderItemDialog();
        orderItemDialog.setOrderItemListener(this);
        orderItemDialog.setArguments(bundle);
        orderItemDialog.show(getFragmentManager(), "loadOrderItem");
    }

    @Override
    public void OnCategoryClick(String category) {
        currentCategory = category;

        ArrayList<Product> catProducts = new ArrayList<>();
        for (Product product : main.products) {
            if (product.getCategory().equals(currentCategory)) {
                catProducts.add(product);
            }
        }
        main.productAdapter = new ProductAdapter(catProducts);
        main.productAdapter.setProductItemListener(this);

        productGrid.setAdapter(main.productAdapter);

        main.productAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnEdit(OrderItem orderItem) {
        currentOrder.calculateTotal();
        totalOrder.setText(String.valueOf(currentOrder.getTotal()));

        orderItemAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnAdd(OrderItem orderItem) {
        currentOrder.addOrderItem(orderItem);
        totalOrder.setText(String.valueOf(currentOrder.getTotal()));

        orderItemAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnRemove(OrderItem orderItem) {
        currentOrder.removeOrderItem(orderItem);
        totalOrder.setText(String.valueOf(currentOrder.getTotal()));

        orderItemAdapter.notifyDataSetChanged();
    }

    public interface OrderListener {
        void OnSaveClose(Order order);

        void OnPrint(Order order);

        void OnCheckOut(Order order);
    }
}
